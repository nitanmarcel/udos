/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>
#include <QDebug>
#include <QFile>
#include <QString>

#include "qhttpserver.hpp"
#include "qhttpserverconnection.hpp"
#include "qhttpserverrequest.hpp"
#include "qhttpserverresponse.hpp"

#include <map>
#include <stdlib.h>

std::map<std::string, std::string> mimes;



int main(int argc, char *argv[])
{   

    
    QGuiApplication::setApplicationName("UDos");
    QGuiApplication::setOrganizationName("udos.nitanmarcel");
    QGuiApplication::setApplicationName("udos.nitanmarcel");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    QGuiApplication *app = new QGuiApplication(argc, (char**)argv);
    QQuickStyle::setStyle("Suru");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("availableStyles", QQuickStyle::availableStyles());
    engine.load(QUrl("qml/Main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    mimes[".html"] = "text/html";
    mimes[".css"] = "text/css";
    mimes[".js"] = "application/javascript";
    mimes[".wasm"] = "application/wasm";
    mimes[".png"] = "image/png";
    mimes[".icon"] = "image/x-icon";
    mimes[".json"] = "application/json";

    qhttp::server::QHttpServer server;
    server.listen(QHostAddress::LocalHost, 15469,
        [](qhttp::server::QHttpRequest *req, qhttp::server::QHttpResponse *res)
        {
            QString docname = "./www/" + (req->url().toString()==("/") ?("/index.html"):req->url().toString());
            docname.replace("./www//undefined/", "");
            if (!QFile(docname).exists())
                docname = QString("./www/index.html");
            QFile doc(docname);
            doc.open(QFile::ReadOnly);

            res->addHeader("Content-Length", QString::number(doc.size()).toUtf8());
            res->addHeader("Connection", "keep-alive");

            auto doc_str = docname.toStdString();
            auto doc_ext = doc_str.substr(doc_str.find_last_of('.'));
            if (mimes.count(doc_ext) > 0) {
                res->addHeader("Content-Type", mimes[doc_ext].data());
            }
                
            else
                res->addHeader("Content-Type", "application/octet-stream");
            res->setStatusCode(qhttp::TStatusCode::ESTATUS_OK);
            res->write(doc.readAll());
        });


    return app->exec();
}
