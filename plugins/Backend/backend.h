/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * backend is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QSqlDatabase>

class Backend : public QObject
{
    Q_OBJECT
    Q_INVOKABLE void initDatabase();

public:
    Backend();
    ~Backend() = default;

    Q_INVOKABLE bool isDatabaseOpen();
    Q_INVOKABLE bool createDatabaseTable();
    Q_INVOKABLE bool addBundle(QString title);
    Q_INVOKABLE bool removeBundle(QString title);
    Q_INVOKABLE QStringList getAllBundles();
    Q_INVOKABLE bool bundleExists(QString title);
    Q_INVOKABLE bool copyBundle(QString fromPath, QString title);
    Q_INVOKABLE QString getBundlePath(QString fromTitle);
    Q_INVOKABLE QString formatName(QString originalName, bool toJdos);
    Q_INVOKABLE bool isValidBundle(QString bundlePath);
    Q_INVOKABLE QString getDownloadDir();

    Q_INVOKABLE void setShowDelegateHelper(bool state);
    Q_INVOKABLE bool getShowDelegateHelper();
    Q_INVOKABLE QString getBackupKey();
    Q_INVOKABLE void setBackupKey(QString backupKey);
    Q_INVOKABLE QString generateBackupKey();

    Q_INVOKABLE QString replaceBetween(QString text, QString replacement, int start, int end);
    Q_INVOKABLE QString insertAt(QString text, QString textIn, int pos);

private:
    QSqlDatabase m_db;
};

#endif
