import QtQuick 2.7
import QtQuick.Controls 2.5

import QtQuick.Layouts 1.3
import QtSystemInfo 5.5
import QtWebEngine 1.8
import QtWebChannel 1.0

import Backend 1.0

Page {
    id: playerPage
    property string bundleUrl;
    title: "Player"

    WebEngineView {
        id: webView
        anchors.fill: parent
        url : "http://localhost:15469/"
        webChannel: channel

        settings.accelerated2dCanvasEnabled: true

        profile: WebEngineProfile {
            httpUserAgent: "Mozilla/5.0 (Linux; Android 11; SM-A125U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Mobile Safari/537.36"
            storageName: "Storage"
            persistentStoragePath: "/home/phablet/.local/share/udos.nitanmarcel/udos.nitanmarcel/QWebEngine"

            onDownloadRequested: (download) => {
                let fileName = download.path.replace(/^.*[\\\/]/, '')
                download.path = Backend.getDownloadDir() + fileName
                console.log("Download requested to: " + download.path)
                download.accept()
            }
            onDownloadFinished: (download) => {
                let exportPage = stackView.push("ExportPage.qml", {"url": download.path})
            }
        }

        onFullScreenRequested: (request) => {
            window.isFullscreenState = !window.isFullscreenState
            request.accept()
        }

        WebChannel {
            id: channel
            registeredObjects: [webChannelObject]
        }

        QtObject {
            id: webChannelObject
            WebChannel.id: "webChannelBackend"
            property alias jsDosFile: playerPage.bundleUrl
            property string backupKey: Backend.getBackupKey()

            signal requestClose()
            function closeEmulator() {
                stackView.pop()
            }
        }
    }

    Rectangle {
        id: webViewLoadingIndicator
        anchors.fill: parent
        z: 1
        visible: webView.loading === true

        BusyIndicator {
            id: busy
            anchors.centerIn: parent
        }
    }

    ScreenSaver {
        id: screenSaver
        screenSaverEnabled: !(Qt.application.active)
    }

    function close() {
        webChannelObject.requestClose()
    }
}
