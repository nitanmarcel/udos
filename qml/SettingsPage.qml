/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.5

import QtQuick.Layouts 1.3
import "./components" as Custom

import Backend 1.0


ScrollablePage {
    title: i18n.tr("Settings")

    footer: Label {
            horizontalAlignment: Qt.AlignHCenter
            wrapMode: Label.Wrap
            textFormat: Text.RichText
            text: i18n.tr("Run DOS programs in Ubuntu Touch.<br>© Marcel Alexandru Nitan GPLv3<br><br>Powered by <a href=\"https://js-dos.com/\">js-dos</a>")
            onLinkActivated: {
                Qt.openUrlExternally(link)
            }
        }

    Column {
        spacing: 40
        width: parent.width
        Layout.alignment: Qt.AlignCenter

        Label {
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: i18n.tr("A backup key will allow you to save the game progress in the cloud so you can restore it later.<br>Do not share the backup key with anyone as the saves can be overwritten.")
            textFormat: Text.RichText
        }

        Frame {
            anchors.horizontalCenter: parent.horizontalCenter
            Column {
                spacing: 20
                Custom.TextField {
                    id: backupKeyInput
                    selectByMouse: true
                    text: Backend.getBackupKey()
                    placeholderText: i18n.tr("Enter your backup key.")
                }
                Button {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: backupKeyInput.displayText !== "" ? "Save" : "Genearate"
                    onClicked: {
                        if (backupKeyInput.text !== "") {
                            Backend.setBackupKey(backupKeyInput.text)
                        } else
                        {
                            let key = Backend.generateBackupKey()
                            Backend.setBackupKey(key)
                            backupKeyInput.text = key
                        }
                    }
                }
            }
        }
    }

    function close() {
        stackView.pop()
    }
}