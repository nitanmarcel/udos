/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.5

import QtQuick.Layouts 1.3

import Backend 1.0

Page {
    property string currentBundle
    property bool showDelegateHelper: Backend.getShowDelegateHelper()
    onShowDelegateHelperChanged: Backend.setShowDelegateHelper(false)

    title: "uDos"
    ColumnLayout {
        id: column
        spacing: 40
        anchors.fill: parent
        anchors.topMargin: 20

        Label {
            Layout.fillWidth: true
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            text: i18n.tr("No app available. Press the + sign to add new apps.")
            visible: dosList.count <= 0
        }

        Column {
            spacing: 20
            Layout.alignment: Qt.AlignHCenter
            visible: showDelegateHelper === true && dosList.count > 0
            Label {
                text: i18n.tr("Swipe right to delete. Touch to open.")
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Button {
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Ok"
                highlighted: true
                onClicked: showDelegateHelper = false
            }
        }

        ListView {
            id: dosList
            Layout.fillWidth: true
            Layout.fillHeight: true
            visible: count > 0

            model: ListModel {}
            delegate: SwipeDelegate {
                text: model.title
                width: parent.width
                swipe.left: Button {
                    id: swipeAction
                    anchors.left: parent.left
                    //anchors.centerIn: parent
                    width: units.gu(10)
                    height: parent.height
                    background: Rectangle {
                        color: "#c7162b"
                    }
                    icon.name: "delete"
                    icon.width: units.gu(3)
                    icon.height: icon.width
                    icon.color: "white"

                    onClicked: Backend.removeBundle(model.title) && dosList.refresh()
                }

                onClicked: stackView.push("PlayerPage.qml", {
                    "bundleUrl": Backend.getBundlePath(model.title),
                    "title": model.title
                })
            }
            function refresh() {
                    model.clear();
                    let dosApps = Backend.getAllBundles();
                    dosApps.sort()
                    for (let x in dosApps)
                        model.append({
                        "title" : dosApps[x]})
                }
        }

        Component.onCompleted: {
            Backend.createDatabaseTable();
            dosList.refresh()
            dosListChanged.connect(() => dosList.refresh())
        }
    }
    function close() {
        stackView.pop()
    }
}
