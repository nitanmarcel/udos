/*
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

import QtQuick 2.6
import QtQuick.Controls 2.0
import Ubuntu.Content 1.3

Page {
    id: picker
    title: i18n.tr("Export Save")
	property var activeTransfer

	property var url
	property var handler
	property var contentType
	
    signal cancel()
    signal imported(string fileUrl)

    ContentPeerPicker {
        anchors.fill: parent
        visible: parent.visible
        showTitle: false
        contentType: ContentType.All
        handler: ContentHandler.Share

        onPeerSelected: {
            //peer.selectionType = ContentTransfer.Single
            picker.activeTransfer = peer.request()
            picker.activeTransfer.stateChanged.connect(function() {
                if (picker.activeTransfer.state === ContentTransfer.InProgress) {
                    console.log("In progress " + picker.url);
                    picker.activeTransfer.items = [ resultComponent.createObject(parent, {"url": "file://" + picker.url}) ];
                    picker.activeTransfer.state = ContentTransfer.Charged;
                    stackView.pop()
                }

            })
        }
       

        onCancelPressed: {
            stackView.pop()
        }
    }

    ContentTransferHint {
        id: transferHint
        anchors.fill: parent
        activeTransfer: picker.activeTransfer
    }
    Component {
        id: resultComponent
        ContentItem {}
	}
    
    function close() {
        cancel()
        stackView.pop()
    }
}
