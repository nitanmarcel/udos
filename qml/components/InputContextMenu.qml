/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3 as UITK

import Backend 1.0

Menu {
    id: contextMenu
    focus: false
    property var target
    property bool canCopy: target && target.selectedText !== "" && !contextMenu.password
    property bool password: target && target.hasOwnProperty('echoMode') && target.echoMode == TextInput.Password

    implicitWidth: row.childrenRect.width + padding * 2
    implicitHeight: row.childrenRect.height + padding * 2
    padding: units.gu(1)

    RowLayout {
        id: row
        spacing: 10
        ToolButton {
            spacing: 7
            text: i18n.tr("Select All")
            font.pixelSize: 0.707 * units.dp(14.0)
            font.weight: Font.Thin
            icon.name: "edit-select-all"
            Layout.preferredHeight: units.gu(5)
            display: AbstractButton.TextUnderIcon
            enabled: contextMenu.target && contextMenu.target.text !== "" && contextMenu.target.selectedText === ""
            visible: contextMenu.target && (contextMenu.target.selectedText === "" || contextMenu.password)
            background: Rectangle {
                color: "transparent"
            }
            onClicked: {
                contextMenu.target.selectAll()
            }
        }
        ToolButton {
            spacing: 7
            Layout.preferredHeight: units.gu(5)
            display: AbstractButton.TextUnderIcon
            enabled: !contextMenu.target.readOnly
            visible: contextMenu.canCopy
            background: Rectangle {
                color: "transparent"
            }
            text: i18n.tr("Cut")
            font.pixelSize: 0.707 * units.dp(14.0)
            font.weight: Font.Thin
            icon.name: "edit-cut"
            onClicked: {
                UITK.Clipboard.push(["text/plain", contextMenu.target.selectedText])
                contextMenu.target.text = Backend.replaceBetween(contextMenu.target.text.toString(), "", contextMenu.target.selectionStart, contextMenu.target.selectionEnd)
                contextMenu.dismiss()
            }
        }
        ToolButton {
            spacing: 7
            Layout.preferredHeight: units.gu(5)
            display: AbstractButton.TextUnderIcon
            visible: contextMenu.canCopy
            background: Rectangle {
                color: "transparent"
            }
            text: i18n.tr("Copy")
            font.pixelSize: 0.707 * units.dp(14.0)
            font.weight: Font.Thin
            icon.name: "edit-copy"
            onClicked: {
                UITK.Clipboard.push(["text/plain", contextMenu.target.selectedText])
                contextMenu.dismiss()
            }
        }
        ToolButton {
            spacing: 7
            Layout.preferredHeight: units.gu(5)
            display: AbstractButton.TextUnderIcon
            enabled: contextMenu.target && contextMenu.target.canPaste
            background: Rectangle {
                color: "transparent"
            }
            text: i18n.tr("Paste")
            font.pixelSize: 0.707 * units.dp(14.0)
            font.weight: Font.Thin
            icon.name: "edit-paste"
            onClicked: {
                if (target.selectedText)
                    contextMenu.target.text = Backend.replaceBetween(contextMenu.target.text.toString(), UITK.Clipboard.data.text, contextMenu.target.selectionStart, contextMenu.target.selectionEnd)
                else
                    contextMenu.target.text = Backend.insertAt(contextMenu.target.text.toString(), UITK.Clipboard.data.text, contextMenu.target.cursorPosition)
                contextMenu.dismiss()
            }
        }
    }

}
